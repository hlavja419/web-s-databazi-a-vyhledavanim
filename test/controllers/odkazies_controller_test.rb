require "test_helper"

class OdkaziesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @odkazy = odkazies(:one)
  end

  test "should get index" do
    get odkazies_url
    assert_response :success
  end

  test "should get new" do
    get new_odkazy_url
    assert_response :success
  end

  test "should create odkazy" do
    assert_difference('Odkazy.count') do
      post odkazies_url, params: { odkazy: { název_dokumentu: @odkazy.název_dokumentu, odkaz: @odkazy.odkaz, téma: @odkazy.téma } }
    end

    assert_redirected_to odkazy_url(Odkazy.last)
  end

  test "should show odkazy" do
    get odkazy_url(@odkazy)
    assert_response :success
  end

  test "should get edit" do
    get edit_odkazy_url(@odkazy)
    assert_response :success
  end

  test "should update odkazy" do
    patch odkazy_url(@odkazy), params: { odkazy: { název_dokumentu: @odkazy.název_dokumentu, odkaz: @odkazy.odkaz, téma: @odkazy.téma } }
    assert_redirected_to odkazy_url(@odkazy)
  end

  test "should destroy odkazy" do
    assert_difference('Odkazy.count', -1) do
      delete odkazy_url(@odkazy)
    end

    assert_redirected_to odkazies_url
  end
end
