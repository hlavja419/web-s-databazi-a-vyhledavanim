json.extract! link, :id, :název_dokumentu, :label, :odkaz, :created_at, :updated_at
json.url link_url(link, format: :json)
