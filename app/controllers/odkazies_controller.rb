class OdkaziesController < ApplicationController
  before_action :set_odkazy, only: %i[ show edit update destroy ]

  # GET /odkazies or /odkazies.json
  def index
    @odkazies = Odkazy.all
  end

  # GET /odkazies/1 or /odkazies/1.json
  def show
  end

  # GET /odkazies/new
  def new
    @odkazy = Odkazy.new
  end

  # GET /odkazies/1/edit
  def edit
  end

  # POST /odkazies or /odkazies.json
  def create
    @odkazy = Odkazy.new(odkazy_params)

    respond_to do |format|
      if @odkazy.save
        format.html { redirect_to odkazy_url(@odkazy), notice: "Odkazy was successfully created." }
        format.json { render :show, status: :created, location: @odkazy }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @odkazy.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /odkazies/1 or /odkazies/1.json
  def update
    respond_to do |format|
      if @odkazy.update(odkazy_params)
        format.html { redirect_to odkazy_url(@odkazy), notice: "Odkazy was successfully updated." }
        format.json { render :show, status: :ok, location: @odkazy }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @odkazy.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /odkazies/1 or /odkazies/1.json
  def destroy
    @odkazy.destroy

    respond_to do |format|
      format.html { redirect_to odkazies_url, notice: "Odkazy was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_odkazy
      @odkazy = Odkazy.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def odkazy_params
      params.require(:odkazy).permit(:název_dokumentu, :téma, :odkaz)
    end
end
