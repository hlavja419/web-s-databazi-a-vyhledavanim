class CreateLinks < ActiveRecord::Migration[6.1]
  def change
    create_table :links do |t|
      t.string :název_dokumentu
      t.string :label
      t.string :odkaz

      t.timestamps
    end
  end
end
